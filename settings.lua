-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"

-- include "json" library
json = require('json')

--widget.setTheme( "widget_theme_android" )

--------------------------------------------

gamerestartrequired = false


local settingsExitBtn

local colorone
local colortwo
local colorthree

local settingsDevModeBtn

-- 'onRelease' event listener for playBtn
local function onSettingsExitBtnRelease()
	local parentscene = composer.getSceneName( "current" )
	
	-- save changes to data table
	saveValue('settings.txt', json.encode(settings))
	
	settingsBtn:setEnabled( false )
	settingsExitBtn:setEnabled( false )

	-- transition back to menu
	
		transition.to( overlaybg, { delay=0, time=600, alpha=0 } )
		transition.to( settingsBtn, { delay=0, time=900, rotation=180, transition=easing.inOutBack } )
		
		-- settingsWindowGroup.height = screenH-screenH*0.125-buttonsize
		-- settingsWindowGroup.y = screenH-buttonsize
		-- settingsWindowGroup.yScale = 0.0001
		transition.to( settingsWindowGroup, { delay=0, time=600, alpha=0 } )
		transition.to( settingsWindowArrow, { delay=0, time=600, alpha=0 } )
		transition.to( settingsWindowGroup, { delay=0, time=900, yScale=0.0001, y=screenH-buttonsize, transition=easing.inOutBack  } )
		transition.to( settingsBtn, { delay=300, time=600, alpha=0} )
		settingsExitBtn.alpha=0

	-- hide overlay
	timer.performWithDelay( 900, function() composer.hideOverlay(); end)

	
	settingsButtonShow()
	
	return true	-- indicates successful touch
end

-- 'onRelease' event listener for playBtn
local function onColorBtnRelease( event )
	local newx = 0
		
	if event.target.colornumber == 1 then
		newx = colorone.x
		settings.themeaccentcolor = getColor("settingsone")
	elseif event.target.colornumber == 2 then
		newx = colortwo.x
		settings.themeaccentcolor = getColor("settingstwo")
	elseif event.target.colornumber == 3 then
		newx = colorthree.x
		settings.themeaccentcolor = getColor("settingsthree")
	end
	
	gamerestartrequired = true
	
	-- animation
	transition.to( coloroutline, { delay=0, time=200, x=newx+coloroutlinethickness*0.5, transition=easing.outExpo} )
	transition.to( coloroutline, { delay=0, time=0.5, alpha=0.5, transition=easing.outExpo  } )
	transition.to( coloroutline, { delay=100, time=100, alpha=1, transition=easing.outExpo } )
	
	return true	-- indicates successful touch
end

local tapcount = 0
-- 'onRelease' event listener for playBtn
local function onSettingsDevModeBtnRelease()
	if settings.devmode == 0 then
		if tapcount <= 9 then
			tapcount = tapcount + 1
		else
			tapcount = 0
			settings.devmode = 1
			-- save changes to data table
			saveValue('settings.txt', json.encode(settings))
			print("devmode enabled")
			-- this animation could be cleaned up
			transition.to( settingsDevModeBtn, { delay=0, time=1000, rotation=360, transition=easing.inOutBack } )
			transition.to( settingsDevModeBtn, { delay=1000, time=250, alpha=1 } )
			transition.to( settingsDevModeBtn, { delay=1250, time=250, alpha=0.5 } )
			transition.to( settingsDevModeBtn, { delay=1500, time=250, alpha=1 } )
			transition.to( settingsDevModeBtn, { delay=1750, time=250, alpha=0.5 } )
			transition.to( settingsDevModeBtn, { delay=2000, time=250, alpha=1 } )
			transition.to( settingsDevModeBtn, { delay=2250, time=250, alpha=0.5 } )
			transition.to( settingsDevModeBtn, { delay=2000, time=1, rotation=0 } )
			-- refreshes the version text in main.lua
			settingsButtonHide()
		end
	else
		if tapcount <= 9 then
			tapcount = tapcount + 1
		else
			tapcount = 0
			settings.devmode = 0
			-- save changes to data table
			saveValue('settings.txt', json.encode(settings))
			print("devmode disabled")
			transition.to( settingsDevModeBtn, { delay=0, time=250, alpha=1 } )
			transition.to( settingsDevModeBtn, { delay=250, time=250, alpha=0.5 } )
			settingsButtonHide()
		end
	end
	return true	-- indicates successful touch
end




function scene:create( event )
	local sceneGroup = self.view

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.
	
	-- background
	overlaybg = display.newRect( 0, 0, screenW, screenH )
	overlaybg.anchorX = 0
	overlaybg.anchorY = 0
	overlaybg:setFillColor( getColor("bgcolor") )
	overlaybg.alpha = 0
	
	-- settings window group
	settingsWindowGroup = display.newGroup()
	settingsWindowGroup.anchorX = 0
	settingsWindowGroup.anchorY = 0
	settingsWindowGroup.x = 0
	settingsWindowGroup.y = 0
	settingsWindowGroup.alpha = 0
	
	-- text background
	settingsTextbg = display.newRect( 0, screenH*0.125, screenW, screenH*0.125)
	settingsTextbg:setFillColor( getColor("darkforecolor") )
	settingsTextbg.anchorX = 0
	settingsTextbg.anchorY = 0
	
	-- "Settings" Text
	settingsText = display.newText("Settings", halfW, settingsTextbg.height*1.5, fontface, 32) 
	settingsText:setFillColor( getColor("forecolor") )
	
	-- window background
	settingsWindowbg = display.newRect( 0, settingsTextbg.y+settingsTextbg.height, screenW, screenH-buttonsize-settingsTextbg.height*2)
	settingsWindowbg:setFillColor( getColor("darkforecolor") )
	settingsWindowbg.anchorX = 0
	settingsWindowbg.anchorY = 0
	
	
	-- scrollView listener
	local function scrollListener( event )
		local phase = event.phase
		local direction = event.direction
		
		if "began" == phase then
			--print( "Began" )
			transition.to( scrollviewshadowbottom, { delay=0, time=200, alpha=0.125, height=halfH*0.075 } )
			transition.to( scrollviewshadowtop, { delay=0, time=200, alpha=0.125, height=halfH*0.075 } )
		elseif "moved" == phase then
			--print( "Moved" )
		elseif "ended" == phase then
			--print( "Ended" )
			transition.to( scrollviewshadowbottom, { delay=0, time=200, alpha=0.1, height=halfH*0.05 } )
			transition.to( scrollviewshadowtop, { delay=0, time=200, alpha=0.1, height=halfH*0.05 } )
		end
		
		-- If the scrollView has reached its scroll limit
		if event.limitReached then
			if "up" == direction then
				--print( "Reached Top Limit" )
			elseif "down" == direction then
				--print( "Reached Bottom Limit" )
			elseif "left" == direction then
				--print( "Reached Left Limit" )
			elseif "right" == direction then
				--print( "Reached Right Limit" )
			end
		end

		return true
	end
	
	-- Create a scrollView
	scrollView = widget.newScrollView {
		left = 0,
		top = settingsTextbg.height*2,
		width = screenW,
		height = screenH-settingsTextbg.height*2.25-buttonsize,
		topPadding = 0,
		scrollWidth = 1,
		hideBackground = true,
		hideScrollBar = true,
		backgroundColor = { getColor("darkforecolor") },
		isBounceEnabled = true,
		horizontalScrollDisabled = true,
		verticalScrollDisabled = false,
		listener = scrollListener,
	}
	
	-- shadows for scrollview
	scrollviewshadowtop = display.newRect( scrollView.x, scrollView.y-scrollView.height*0.5, screenW, halfH*0.05)
	scrollviewshadowtop.fill = {type = "gradient", color1 = { getColor("bgcolor") }, color2 = { 0,0 }, direction = "down"} -- gradient
	scrollviewshadowtop.anchorX = 0.5
	scrollviewshadowtop.anchorY = 0
	scrollviewshadowtop.alpha=0.1
	scrollviewshadowbottom = display.newRect( scrollView.x, scrollView.y+scrollView.height*0.5, screenW, halfH*0.05)
	scrollviewshadowbottom.fill = {type = "gradient", color1 = { getColor("bgcolor") }, color2 = { 0,0 }, direction = "up"} -- gradient
	scrollviewshadowbottom.anchorX = 0.5
	scrollviewshadowbottom.anchorY = 1
	scrollviewshadowbottom.alpha=0.1
	
	---------------------------------------------------------------------------------------------------------------
	
	local spacing = {
		top = 32,
		bottom = 16,
		right = 72,
		left = 24,
		between = 40,
	}
	
	
	-- Sound
	
	local function SoundonOffSwitchListener( event )
		if event.target.isOn then
			settings.soundmode = 1
		else
			settings.soundmode = 0
		end
	end
	
	local SoundonOffText = display.newText("Sound", spacing.left, spacing.top, fontface, 24) 
	SoundonOffText:setFillColor( getColor("forecolor") )
	SoundonOffText.anchorX = 0
	SoundonOffText.anchorY = 0.5
	scrollView:insert( SoundonOffText )

	SoundonOffSwitch = widget.newSwitch
		{
			onRelease = SoundonOffSwitchListener,
		}
	SoundonOffSwitch.anchorX = 0
	SoundonOffSwitch.anchorY = 0.5
	SoundonOffSwitch.x = screenW - spacing.right
	SoundonOffSwitch.y = spacing.top
	scrollView:insert( SoundonOffSwitch )
	
	-- Experimental
	
	local experimentalText = display.newText("- Experimental -", screenW/2, SoundonOffText.y+spacing.between*1.25, fontface, 20) 
	experimentalText:setFillColor( getColor("forecolor") )
	experimentalText.alpha = 0.5
	experimentalText.anchorX = 0.5
	experimentalText.anchorY = 0.5
	scrollView:insert(experimentalText)
	
	-- Vibrate Mode
	
	local function VibrationonOffSwitchListener( event )
		if event.target.isOn then
			settings.vibratemode = 1
		else
			settings.vibratemode = 0
		end
	end
	
	local VibrationonOffText = display.newText("Vibrate Mode", spacing.left, experimentalText.y+spacing.between, fontface, 24) 
	VibrationonOffText:setFillColor( getColor("forecolor") )
	VibrationonOffText.anchorX = 0
	VibrationonOffText.anchorY = 0.5
	scrollView:insert(VibrationonOffText)

	VibrationonOffSwitch = widget.newSwitch
		{
			onRelease = VibrationonOffSwitchListener,
		}
	VibrationonOffSwitch.anchorX = 0
	VibrationonOffSwitch.anchorY = 0.5
	VibrationonOffSwitch.x = screenW - spacing.right
	VibrationonOffSwitch.y = experimentalText.y+spacing.between
	scrollView:insert( VibrationonOffSwitch )
	
	
	-- -- Change Color
	
	-- local ThemeColorText = display.newText("- Theme Color -", screenW/2, VibrationonOffText.y+spacing.between*1.25, fontface, 20) 
	-- ThemeColorText:setFillColor( getColor("forecolor") )
	-- ThemeColorText.alpha = 0.5
	-- ThemeColorText.anchorX = 0.5
	-- ThemeColorText.anchorY = 0.5
	-- scrollView:insert(ThemeColorText)
	
	-- -- color picker
	
	-- local colorbuttonsize = (screenW-spacing.left*2)/3
	
	-- colorone = widget.newButton{
		-- label="",
		-- shape="Rect",
		-- emboss = false,
		-- cornerRadius = 2,
		-- labelColor = { default={ getColor("forecolor") }, over={ getColor("forecolor") } },
		-- fillColor = { default={ getColor("settingscolorone") }, over={ getColor("settingscolorone") } },
		-- width=colorbuttonsize, height=colorbuttonsize*0.5,
		-- onRelease = onColorBtnRelease	-- event listener function
	-- }
	-- colorone.x = spacing.left
	-- colorone.y = ThemeColorText.y + spacing.between - 12
	-- colorone.anchorX = 0
	-- colorone.anchorY = 0
	-- colorone.colornumber = 1
	-- scrollView:insert( colorone )
	
	-- colortwo = widget.newButton{
		-- label="",
		-- shape="Rect",
		-- emboss = false,
		-- cornerRadius = 2,
		-- labelColor = { default={ getColor("forecolor") }, over={ getColor("forecolor") } },
		-- fillColor = { default={ getColor("settingscolortwo") }, over={ getColor("settingscolortwo") } },
		-- width=colorbuttonsize, height=colorone.height,
		-- onRelease = onColorBtnRelease	-- event listener function
	-- }
	-- colortwo.x = colorone.x + colorbuttonsize
	-- colortwo.y = colorone.y
	-- colortwo.anchorX = 0
	-- colortwo.anchorY = 0
	-- colortwo.colornumber = 2
	-- scrollView:insert( colortwo )
	
	-- colorthree = widget.newButton{
		-- label="",
		-- shape="Rect",
		-- emboss = false,
		-- cornerRadius = 2,
		-- labelColor = { default={ getColor("forecolor") }, over={ getColor("forecolor") } },
		-- fillColor = { default={ getColor("settingscolorthree") }, over={ getColor("settingscolorthree") } },
		-- width=colorbuttonsize, height=colorone.height,
		-- onRelease = onColorBtnRelease	-- event listener function
	-- }
	-- colorthree.x = colortwo.x + colorbuttonsize
	-- colorthree.y = colorone.y
	-- colorthree.anchorX = 0
	-- colorthree.anchorY = 0
	-- colorthree.colornumber = 3
	-- scrollView:insert( colorthree )
	
	-- coloroutlinethickness = 4
	-- coloroutline = display.newRect( colorone.x+coloroutlinethickness*0.5, colorone.y-coloroutlinethickness*0.5, colorbuttonsize-coloroutlinethickness, colorone.height+coloroutlinethickness )
	-- coloroutline:setFillColor( 0, 0, 0, 0 )
	-- coloroutline:setStrokeColor( getColor("forecolor") )
	-- coloroutline.strokeWidth = coloroutlinethickness
	-- coloroutline.anchorX = 0
	-- coloroutline.anchorY = 0
	-- scrollView:insert( coloroutline )
	
	
	-- Info
	local timesplayedstring = string.format("Times Played: %s", data.statTimesPlayed)
	local timesplayedtext = display.newText( timesplayedstring, spacing.left, screenH*0.75, fontface, 16) 
	timesplayedtext:setFillColor( getColor("forecolor") )
	timesplayedtext.anchorX = 0
	timesplayedtext.anchorY = 0
	timesplayedtext.alpha = 0.5
	scrollView:insert( timesplayedtext )
	
	local versiontext = "null"
	if system.getInfo( "environment" ) == "simulator" then
		versiontext = "Version: Simulator Preview"
		modeltext = "Device: Simulator Preview\n   "
	else 
		versiontext = string.format("Version: %s", system.getInfo("appVersionString"))
		modeltext = string.format("Device: %s [%s]\n   ", system.getInfo("model"), system.getInfo("architectureInfo"))
	end
	
	-- info version text
	
	local infoversiontext = display.newText( versiontext, spacing.left, timesplayedtext.y+spacing.between, fontface, 16) 
	infoversiontext:setFillColor( getColor("forecolor") )
	infoversiontext.anchorX = 0
	infoversiontext.anchorY = 0.5
	infoversiontext.alpha = 0.5
	scrollView:insert( infoversiontext )
	
	local infomodeltext = display.newText( modeltext, spacing.left, infoversiontext.y+spacing.between*0.2, fontface, 16) 
	infomodeltext:setFillColor( getColor("forecolor") )
	infomodeltext.anchorX = 0
	infomodeltext.anchorY = 0
	infomodeltext.alpha = 0.5
	scrollView:insert( infomodeltext )
	
	settingsDevModeBtn = widget.newButton{
		label="Developed by\nNostabyteGames.com",
		font=fontface,
		fontSize = 16,
		labelYOffset = 1,
		shape="Rect",
		emboss = false,
		cornerRadius = 2,
		labelColor = { default={ getColor("forecolor") }, over={ getColor("forecolor") } },
		fillColor = { default={ getColor("darkforecolor") }, over={ getColor("darkforecolor") } },
		width=buttonsize, height=buttonsize*0.75,
		onRelease = onSettingsDevModeBtnRelease	-- event listener function
	}
	settingsDevModeBtn.x = spacing.left + settingsDevModeBtn.width*0.5
	settingsDevModeBtn.y = infomodeltext.y + spacing.between*0.5 + settingsDevModeBtn.height*0.5
	settingsDevModeBtn.anchorX = 0.5
	settingsDevModeBtn.anchorY = 0.5
	settingsDevModeBtn.alpha = 0.5
	scrollView:insert( settingsDevModeBtn )
	
	
	-- set switches to match the current settings
	
	if settings.soundmode == 0 then
		SoundonOffSwitch:setState( { isOn=false, isAnimated=false, onComplete=SoundonOffSwitchListener } )
	else 
		SoundonOffSwitch:setState( { isOn=true, isAnimated=false, onComplete=SoundonOffSwitchListener } )
	end
	
	if settings.vibratemode == 0 then
		VibrationonOffSwitch:setState( { isOn=false, isAnimated=false, onComplete=VibrationonOffSwitchListener } )
	else 
		VibrationonOffSwitch:setState( { isOn=true, isAnimated=false, onComplete=VibrationonOffSwitchListener } )
	end
	
	-- set color 
	-- if getColor("settingscolorone") == settings.themeaccentcolor then
		-- coloroutline.x = colorone.x+coloroutlinethickness*0.5
	-- elseif getColor("settingscolortwo") == settings.themeaccentcolor then
		-- coloroutline.x = colortwo.x+coloroutlinethickness*0.5
	-- elseif getColor("settingscolorthree") == settings.themeaccentcolor then
		-- coloroutline.x = colorthree.x+coloroutlinethickness*0.5
	-- end
	
	---------------------------------------------------------------------------------------------------------------
	
	settingsBtn = widget.newButton{
		label="",
		defaultFile = "settingsicon.png",
		overFile = "settingsicon.png",
		emboss = false,
		cornerRadius = 2,
		width=buttonsize*0.5, height=buttonsize*0.5,
		onRelease = onSettingsExitBtnRelease	-- event listener function
	}
	settingsBtn.anchorX = 0.5
	settingsBtn.anchorY = 0.5
	settingsBtn.x = screenW-buttonsize*0.5
	settingsBtn.y = screenH-buttonsize*0.5
	settingsBtn.alpha = 1
	
	settingsExitBtn = widget.newButton{
		label="",
		font=fontface,
		fontSize = 28,
		labelYOffset = 1,
		shape="Rect",
		emboss = false,
		cornerRadius = 2,
		labelColor = { default={ getColor("darkforecolor") }, over={ getColor("bgcolor") } },
		fillColor = { default={ getColor("darkforecolor") }, over={ getColor("bgcolor") } },
		width=buttonsize*0.75, height=buttonsize*0.75,
		onRelease = onSettingsExitBtnRelease	-- event listener function
	}
	settingsExitBtn.anchorX = 0.5
	settingsExitBtn.anchorY = 0.5
	settingsExitBtn.x = screenW-buttonsize*0.5
	settingsExitBtn.y = settingsTextbg.height*1.5
	settingsExitBtn.alpha = 0
	
	local exitvertices = { 0,4, 8,12, 12,8, 4,0, 12,-8, 8,-12, 0,-4, -8,-12, -12,-8, -4,0, -12,8, -8,12 }
	settingsWindowExit = display.newPolygon( settingsExitBtn.x, settingsExitBtn.y, exitvertices)
	settingsWindowExit:setFillColor( getColor("forecolor") )
	settingsWindowExit.anchorY = 0.5
	settingsWindowExit.alpha = 1
	
	local trivertices = { buttonsize*0.125,0, 0,buttonsize*0.125, -buttonsize*0.125,0 , }
	settingsWindowArrow = display.newPolygon( settingsBtn.x, settingsBtn.y-buttonsize*0.5, trivertices)
	settingsWindowArrow:setFillColor( getColor("darkforecolor") )
	settingsWindowArrow.anchorY = 0
	settingsWindowArrow.alpha = 0
	
	
	-- all display objects must be inserted into group
	sceneGroup:insert( overlaybg )
	settingsWindowGroup:insert( settingsWindowbg )
	settingsWindowGroup:insert( settingsTextbg )
	settingsWindowGroup:insert( settingsText )
	settingsWindowGroup:insert( settingsExitBtn )
	settingsWindowGroup:insert( settingsWindowExit )
	settingsWindowGroup:insert( scrollView )
	settingsWindowGroup:insert( scrollviewshadowtop )
	settingsWindowGroup:insert( scrollviewshadowbottom )
	sceneGroup:insert( settingsWindowArrow )
	sceneGroup:insert( settingsWindowGroup )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		
			-- animations
			
			settingsBtn:setEnabled( false )
			timer.performWithDelay( 900, function() settingsBtn:setEnabled( true ); end)
			
			transition.to( overlaybg, { delay=300, time=600, alpha=0.5 } )
			
			transition.to( settingsBtn, { delay=0, time=900, rotation=-180, transition=easing.inOutBack } )
			
			settingsWindowGroup.height = screenH-screenH*0.125-buttonsize
			settingsWindowGroup.y = screenH-buttonsize
			settingsWindowGroup.yScale = 0.0001
			transition.to( settingsWindowGroup, { delay=300, time=300, alpha=1 } )
			transition.to( settingsWindowArrow, { delay=300, time=300, alpha=1 } )
			transition.to( settingsWindowGroup, { delay=0, time=900, yScale=1, y=0, transition=easing.inOutBack  } )
			transition.to( settingsExitBtn, { delay=900, time=1, alpha=0.25 } )
			
			settingsButtonHide()
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.

	if settingsBtn then
		settingsBtn:removeSelf()	-- widgets must be manually removed
		settingsBtn = nil
	end 
	
	if settingsExitBtn then
		settingsExitBtn:removeSelf()	-- widgets must be manually removed
		settingsExitBtn = nil
	end 
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene