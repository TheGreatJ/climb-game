-----------------------------------------------------------------------------------------
--
-- refill.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"

local physics = require( "physics" )
physics.start()

-- include "json" library
json = require('json')

local ads = require("ads")

-- debug
physics.setDrawMode( "normal" )

speedupisenabled = true


----------------------------------------------
-- $$$$$$$$$  Admob  $$$$$$$$$ --
----------------------------------------------

bannerID =     "ca-app-pub-9533789630564860/1835926839"
interstitialID = "ca-app-pub-9533789630564860/3312660039"
adTestMode = false
interstitialFetchFlag = 1 -- Call Load Interstitial if this Flag is set to 1

function adListener( event )
   
    -- The 'event' table includes:
    -- event.name: string value of "adsRequest"
    -- event.response: message from the ad provider about the status of this request
    -- event.phase: string value of "loaded", "shown", or "refresh"
    -- event.type: string value of "banner" or "interstitial"
    -- event.isError: boolean true or false
	
	for k,v in pairs( event ) do
		print("adListener ", k, v ) -- debug
	end
	
	if (event.type == "banner") then
		if ( event.isError ) then
			print( "Banner Ad Error" )
			speedupisenabled = false
			transition.to( speedupBtnGroup, { delay=200, time=500, y=halfH, transition=easing.inExpo } )
			transition.to( speedupBtnGroup, { delay=200, time=250, alpha=0 } )
		else
			print( "Banner Ad Loaded" )
			speedupisenabled = true
			transition.to( speedupBtnGroup, { delay=200, time=250, alpha=1} )
		end
		
	elseif (event.type == "interstitial") then
		if ( event.isError ) then
			print( "Interstitial Ad Error" )
			speedupisenabled = false
			speedupBtn:setLabel( "Error" )
		elseif ( event.phase == "loaded" ) then
			-- an ad was preloaded
			interstitialFetchFlag = 0
		elseif ( event.phase == "shown" ) then
			-- the ad was viewed and closed
			print("interstitial-closed shown")
			speeduprefill()
		end
	end
	
end

ads.init("admob", interstitialID, adListener )
ads:setCurrentProvider("admob")
ads.load("admob", { appId = bannerID, testMode = adTestMode } )

local yPos = -(display.actualContentHeight - display.contentHeight)*0.5
ads.show( "banner", { x = 0, y = yPos, appId = bannerID, testMode = adTestMode } )
--ads.show( "interstitial", { appId = interstitialID, testMode = adTestMode } )

----------------------------------------------


----------------
-- Variables --
----------------

local refillspeed
local speedupBtn

-- 'onRelease' event listener for SpeedupBtn
local function onspeedupBtnRelease()
	if composer.getScene("gameover") == nil then --wait untill gameover scene is destroyed
		if speedupisenabled then
		
		ads.load("admob", { appId = interstitialID, testMode = adTestMode } )
		
		-- show the ad
		ads.show( "interstitial", { appId = interstitialID, testMode = adTestMode } )
		
		speedupBtn:setLabel("Loading...")
		
		end
	end
end


function scene:create( event )
	local sceneGroup = self.view

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.
	
	-- drop group
	dropGroup = display.newGroup()
	dropGroup.anchorX = 0
	dropGroup.anchorY = 0
	dropGroup.x = 0
	dropGroup.y = 0
	dropGroup.alpha = 0
	
	dropParticleSystem = physics.newParticleSystem(
	   {
		  filename = "particle.png",
		  radius = 3,
		  imageRadius = 3
	   }
	)
	dropGroup:insert( dropParticleSystem )
	
	-- life tank group
	lifetankGroup = display.newGroup()
	lifetankGroup.anchorX = 0
	lifetankGroup.anchorY = 0
	lifetankGroup.x = 0
	lifetankGroup.y = 0
	lifetankGroup.alpha = 0
	
	lifetankborderGroup = display.newGroup()
	lifetankborderGroup.anchorX = 0
	lifetankborderGroup.anchorY = 0
	lifetankborderGroup.x = 0
	lifetankborderGroup.y = 0
	lifetankborderGroup.alpha = 1
	
	-- border
	tankborderthickness = 2
	lifetankbordertop = display.newRect( halfW, halfH-screenH*0.25+tankborderthickness*0.5, screenW*0.8, tankborderthickness )
	lifetankbordertop:setFillColor( getColor("darkforecolor") )
	lifetankborderright = display.newRect( screenW*0.9-tankborderthickness*0.5, halfH, tankborderthickness, screenH*0.5 )
	lifetankborderright:setFillColor( getColor("darkforecolor") )
	lifetankborderbottom = display.newRect( halfW, halfH+screenH*0.25-tankborderthickness*0.5, screenW*0.8, tankborderthickness )
	lifetankborderbottom:setFillColor( getColor("darkforecolor") )
	lifetankborderleft = display.newRect( screenW*0.1+tankborderthickness*0.5, halfH, tankborderthickness, screenH*0.5 )
	lifetankborderleft:setFillColor( getColor("darkforecolor") )
	-- extra border to hide particles
	lifetankborderbottomextra = display.newRect( halfW, halfH+screenH*0.25+4, screenW*0.8, 8 )
	lifetankborderbottomextra:setFillColor( getColor("bgcolor") )
	
	lifetankborderGroup:insert( lifetankbordertop )
	lifetankborderGroup:insert( lifetankborderright )
	lifetankborderGroup:insert( lifetankborderbottomextra )
	lifetankborderGroup:insert( lifetankborderbottom )
	lifetankborderGroup:insert( lifetankborderleft )
	
	-- invisible physics borders
	physicsborderright = display.newRect( lifetankborderright.x, halfH, tankborderthickness, screenH )
	physicsborderleft = display.newRect( lifetankborderleft.x, halfH, tankborderthickness, screenH )
	physicsborderbottom = display.newRect( halfW, lifetankborderbottom.y+12 , screenW, 20 )
	physicsborderright:setFillColor( 1, 0, 0, 0 )
	physicsborderleft:setFillColor( 1, 0, 0, 0 )
	physicsborderbottom:setFillColor( 1, 0, 0, 0 )

	physics.addBody( physicsborderright, "static" )
	physics.addBody( physicsborderleft, "static" )
	physics.addBody( physicsborderbottom, "static" )
	
	-- Lives
	LivesText = display.newText("Generating Lives", halfW, halfH-28, fontface, 18)
	LivesText:setFillColor( getColor("forecolor") )
	lifetankGroup:insert( LivesText )
	
	Lives = display.newText("0.0%", halfW, halfH+8, fontface, 40)
	Lives:setFillColor( getColor("forecolor") )
	lifetankGroup:insert( Lives )
	
	doneText = display.newText("Refill Complete", halfW, halfH, fontface, 24)
	doneText:setFillColor( getColor("forecolor") )
	doneText.alpha = 0
	doneText:scale( 0.9, 0.9 )
	lifetankGroup:insert( doneText )
	
	-- life tank group
	lifetankareaGroup = display.newGroup()
	lifetankareaGroup.anchorX = 0
	lifetankareaGroup.anchorY = 0
	lifetankareaGroup.x = 0
	lifetankareaGroup.y = 0
	lifetankareaGroup.alpha = 1
	
	-- life tank fill 
	lifetankareafill = display.newRect( halfW, lifetankborderbottom.y-tankborderthickness*0.5, lifetankborderright.x-lifetankborderleft.x-tankborderthickness, 0 )
	lifetankareafill:setFillColor( getColor("accentcolor") )
	lifetankareafill.anchorX = 0.5
	lifetankareafill.anchorY = 1
	lifetankareaGroup:insert( lifetankareafill )
	
	-- speedup button group
	speedupBtnGroup = display.newGroup()
	speedupBtnGroup.anchorX = 0
	speedupBtnGroup.anchorY = 0
	speedupBtnGroup.x = 0
	speedupBtnGroup.y = 0
	speedupBtnGroup.alpha = 0
	
	-- speedup button
	speedupBtn = widget.newButton{
		label="Speed Up",
		font=fontface,
		fontSize = 24,
		labelYOffset = 0,
		shape="Rect",
		emboss = false,
		strokeWidth = 1,
		labelColor = { default={ getColor("forecolor") }, over={ getColor("forecolor") } },
		strokeColor = { default={ getColor("darkforecolor") }, over={ getColor("accentcolor") } },
		fillColor = { default={ getColor("darkforecolor") }, over={ getColor("bgcolor")} },
		width=screenW*0.5, height=buttonsize,
		onRelease = onspeedupBtnRelease	-- event listener function
	}
	speedupBtn.x = halfW
	speedupBtn.y = screenH*0.85
	speedupBtn.alpha = 1
	
	speedupBtnAni = display.newRect( speedupBtn.x, speedupBtn.y, screenW*0.5, buttonsize  )
	speedupBtnAni:setFillColor( getColor("accentcolor") )
	
	speedupBtnGroup:insert( speedupBtn )
	speedupBtnGroup:insert( speedupBtnAni )

	
	
	-- all display objects must be inserted into group
	sceneGroup:insert( lifetankareaGroup )
	sceneGroup:insert( dropGroup )
	sceneGroup:insert( lifetankborderGroup )
	sceneGroup:insert( lifetankGroup )
	sceneGroup:insert( speedupBtnGroup )
end

-----------
-- Refill --
-----------

local function refill( event )

	refilladd = data.playerlivesmax*0.001  -- how much is added
	
	-- if there is room
	if data.currentplayerlives <= data.playerlivesmax-refilladd then
	
		data.currentplayerlives = data.currentplayerlives + refilladd

		lifetankamount = data.currentplayerlives/data.playerlivesmax
		percentstring = lifetankamount*100
		--Lives.text = string.format("%4.1f%%", percentstring )
		Lives.text = string.format("%4.0f%%", percentstring )
		
		-- random droplette spawn
		dropx = (halfW)+(math.random( -16, 16))*(3-(refillspeed*0.02))
		
		dropParticleSystem:createParticle(
		   {
			  flags = { "water" },
			  x = dropx,
			  y = -5,
			  velocityX = 0,
			  velocityY = 500,
			  color = { getColor( "accentcolor" ) },
			  lifetime = 2.5
		   }
		)
		
		-- move invisible bottom border to match tank fill
		if (lifetankborderbottom.y-lifetankbordertop.y)*lifetankamount <= lifetankborderbottom.y-lifetankbordertop.y-15 then
			physicsborderbottom.y = (lifetankareafill.y-(lifetankborderbottom.y-lifetankbordertop.y)*lifetankamount)+15
		end
		
		-- transition lifetankfill to meet new amount
		transition.to( lifetankareafill, { delay=0, time=refillspeed, height=(lifetankborderbottom.y-lifetankbordertop.y)*lifetankamount, transition=easing.outExpo } )
		
		-- putting it here instead of having an infinite timer lets us change the speed during runtime
		refillTimer = timer.performWithDelay( refillspeed, refill, 1 )
		
	else
	
		-- animations
	
			-- fade out text
			transition.to( LivesText, { delay=0, time=300, alpha=0 } )
			transition.to( Lives, { delay=0, time=300, alpha=0 } )
			
			-- fade out drops
			transition.to( dropGroup, { delay=0, time=500, alpha=0 } )
			
			-- refill complete text animation
			transition.to( doneText, { delay=200, time=1000, xScale=1, yScale=1, transition=easing.outElastic } )
			transition.to( doneText, { delay=200, time=200, alpha=1 } )

			lifetankareafill.height=(lifetankborderbottom.y-lifetankbordertop.y)
				
				
			-- fade out speedup button	
			
			transition.to( speedupBtnGroup, { delay=0, time=500, alpha=0, y=halfH, transition=easing.inExpo } )
			
			transition.to( lifetankareaGroup, { delay=1000, time=500, alpha=0 } )
			transition.to( lifetankareafill, { delay=1000, time=500, alpha=0 } )
			transition.to( lifetankGroup, { delay=1500, time=300, alpha=0 } )
			transition.to( lifetankborderGroup, { delay=1000, time=500, alpha=0} )
			
			transition.to( doneText, { delay=1500, time=300, xScale=0.1, yScale=0.1} )
		
		-- just in case
		data.currentplayerlives = data.playerlivesmax
		 
		-- save changes to data table
		saveValue('data.txt', json.encode(data))
		
		-- hide adds
		ads.hide()
		
		-- go to menu scene
		timer.performWithDelay( 1800, function() composer.gotoScene("menu", "crossFade", 300); end)
		
	end
end

function speeduprefill()
	if speedupisenabled then
		
		speedupBtn:setLabel("Speed Up")
	
		-- speed up refill
		refillspeed = refillspeed - refillspeedupamount
		
		-- disable and hide the button when max is reached
		if refillspeed <= refillspeedmax then
			speedupBtn:setLabel("")
			speedupisenabled = false
			speedupBtn:setEnabled( false )
			transition.to( speedupBtnGroup, { delay=200, time=500, y=halfH, transition=easing.inExpo } )
			transition.to( speedupBtnGroup, { delay=200, time=250, alpha=0 } )
		end
		
	else
		speedupBtn:setEnabled( false )
		transition.to( speedupBtnGroup, { delay=200, time=500, y=halfH, transition=easing.inExpo } )
		transition.to( speedupBtnGroup, { delay=200, time=250, alpha=0 } )
	end
end


function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		
			physics.setGravity( 0, 20) -- effects particle bounceyness
				
			-- refill speed variables ( lower = faster )
			refillspeed = 100                  -- starts at refill speed
			refillspeedupamount = 25    -- for each ad it speeds up by refillspeedupamount
			refillspeedmax = 25              -- untill it reaches refill speedupmax
			
			-- destroy gameover scene 
			timer.performWithDelay( 100, function() composer.removeScene("gameover"); end)
			
			-- start refill
			refillTimer = timer.performWithDelay( refillspeed, refill, 1 )
			
			
				-- animations
				
				-- fade in speedup button
				transition.to( speedupBtnAni, { delay=0, time=200, alpha=0 } )
				
				lifetankareaGroup.alpha=0
				lifetankareafill.yScale=0.1
				transition.to( lifetankareaGroup, { delay=0, time=500, alpha=1 } )
				transition.to( lifetankareafill, { delay=0, time=1000, yScale=1, transition=easing.outExpo } )

				lifetankGroup.alpha=0
				transition.to( lifetankGroup, { delay=500, time=500, alpha=1 } )
				
				-- fade in drops
				transition.to( dropGroup, { delay=1400, time=400, alpha=1 } )
				
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	
	if speedupBtn then
		speedupBtn:removeSelf()	-- widgets must be manually removed
		speedupBtn = nil
	end
	
	physics.stop()
	
	-- destroy physics stuff
	package.loaded[physics] = nil
	physics = nil
	
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene